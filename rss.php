<?php
ob_start();
header('Content-Type: text/xml');
$listType = $_GET['list'];
$fileName = $listType . ".txt";
$feedName = str_replace('-', ' ', $listType);
$f = fopen($fileName, "r");
$update = date ("Y-m-d\TH:i:sP", filemtime($fileName)); // gets timestamp from tsv file. I don't think this accounts for time zone
echo "<?xml version='1.0' encoding='utf-8'?>\n<feed xmlns='http://www.w3.org/2005/Atom'>\n<title>" .$feedName . " feed</title>\n<author>\n<name>Jeff Karlsen</name>\n<email>karlsej@scc.losrios.edu</email>\n</author>\n<updated>" . $update ."</updated>\n<id>http://scc.losrios.edu/library/feeds/rss.php?list=" . $listType ."</id>\n<link rel=\"alternate\" href=\"http://scc.losrios.edu/library/feeds/" . $fileName ."\" />\n<link rel=\"self\" href=\"http://scc.losrios.edu/~library/feeds/rss.php?list=" . $listType ."\" />\n";
// adapted from http://stackoverflow.com/a/28478394/1903000
while (($line = fgetcsv($f,20000,"\r")) !== false) {
        $row = $line[0];    // We need to get the actual row (it is the first element in a 1-element array)
        $cells = explode("\t",$row); // tab is used as delimiter. Control character 9 in Sierra export
        $title= htmlspecialchars($cells[0]);
        $title = str_replace(' :', ':', $title);
				$title = str_replace(' ;', ':', $title);
        $title = str_replace('[videorecording]', '', $title);
			 if (strlen($title) > 200) {
            $title = trim(substr($title, 0, 200)) . '...';
            }
        $dateCat = htmlspecialchars($cells[1]);
        $date = strtotime($dateCat);
        $newdate = DateTime::createFromFormat('m-d-Y', $dateCat);
        $prettyDate = $newdate->format('l, F j'); // this is for content part of feed
        $idDate = $newdate->format('Y-m-d');
        $atomDate = $newdate->format('Y-m-d\TH:i:sP');
        $recordNo = htmlspecialchars($cells[2]);
        $recordNo = substr($recordNo, 0, -1); // trims the extra digit in the record numbers that get exported
        $page = htmlspecialchars($cells[3]);
				$page = str_replace('&quot;', '', $page);
				$page = str_replace(':', '', $page);
				if (preg_match('/\.$/', $page) === 0) {
				$page = preg_replace('/$/', '. ', $page);
				}
//        $desc = str_replace('&quot;', '', $desc);
/*        $pageString = preg_match('/\d{2,4} (p\.|pages)/', $desc, $pages); // description includes lots of junk. This is to get just the page numbers
        if ($pages[0] === '') {
            $page = '';
        }
        else {
        $page = $pages[0] . '. ';   
        } */
 //       $summary = htmlspecialchars($cells[4]);
        $summary = htmlspecialchars(html_entity_decode($cells[4], ENT_QUOTES, 'UTF-8'), ENT_NOQUOTES, 'UTF-8'); // reference: http://php.net/manual/en/function.html-entity-decode.php#93378
        $summary = str_replace('"', '', $summary);
        $isbn = htmlspecialchars($cells[5]);
        $singleIB = preg_match('(\d{13})', $isbn, $isbns); // 13-digit works better for openlibrary covers
        $isbn = $isbns[0];
				$year = htmlspecialchars($cells[6]);
//        $year = preg_replace('/[\.|c|\[|\]]/', '', $year);
        $ebscoURL = 'http://search.ebscohost.com/login.aspx?authtype=ip,guest&amp;custid=sacram&amp;groupid=main&amp;profile=eds&amp;direct=true&amp;db=cat01047a&amp;AN=lrois.' . $recordNo . '&amp;site=eds-live&amp;scope=site';
//        $summary = str_replace('&quot;', '', $summary);
				$imgSrc = '';
				if (!empty($isbn)) {
					 $imgSrc = "<a href='" .$ebscoURL . "'><img class='jacket' alt='' src='http://covers.openlibrary.org/b/isbn/" .$isbn ."-M.jpg' /></a>";
				}
        $summary = str_replace('"""', '"', $summary);
        $summary = str_replace('""', '"', $summary);
        if (strlen($summary) > 700) {
            $summary = trim(substr($summary, 0, 700)) . '... <a class="read-more" href="' .$ebscoURL .'">more <abbr>>></abbr></a>';
            }

        echo "<entry> \n";
        echo "<title>" . $title ." (" .$year .")</title> \n";
        echo "<id>tag:scc.losrios.edu," .$idDate . ":/library/feeds/perm.php?record=" .$recordNo ."</id> \n";
        echo "<updated>" .$atomDate ."</updated>\n";
        // content type needs to be xhtml in order not to have to escape all html characters...
        echo "<content type='xhtml'> <div xmlns='http://www.w3.org/1999/xhtml'>" . $imgSrc . "<p class='summary'><span class='updated'>(Added " .$prettyDate . ")</span> " .$page . " " . $summary ."</p></div></content>\n";
        echo "<link href='" .$ebscoURL . "'></link>\n";
        echo "</entry>\n";
}

fclose($f);
echo "\n</feed>"; 
// write to static file - seems like good practice
$contents = ob_get_contents();
$staticFile = $listType . '.xml';
if (is_writable($staticFile)) {
    $fp = fopen($staticFile, 'wb');
    fwrite($fp, $contents);
    fclose($fp);
    }
else {
    die ('not writable');
    }
ob_end_flush(); // doesn't seem to be necessary - for some reason everything is sent anyway...
?>