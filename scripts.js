jQuery(document).ready(function($) {
  var scriptRoot = 'http://scc.losrios.edu/~library/feeds/rss.php?list=';
  $('#newbooks').rssfeed(scriptRoot + 'new-books', {
    limit: 80,
    header: false,
    date: false,
    snippet: false,
    titletag: 'h3'
  });

  $('#newmedia').rssfeed(scriptRoot + 'New-DVDs', {
    limit: 80,
    titletag: 'h3',
    header: false,
    date: false,
    snippet: false
  });

  setTimeout(function() {
    $('#newbooks a').on('click', function() {
      var label = findItemTitle($(this));
      ga('localsiteTracker.send', 'event', 'new books feed', 'click', label);
    });

    $('#newmedia a').on('click', function() {
      var label = findItemTitle($(this));
      ga('localsiteTracker.send', 'event', 'new dvds feed', 'click', label);
    });
  }, 500);

  function findItemTitle(item) {
    var title;
    if ((item.text() !== '') && (item.text() !== undefined)) {
      title = item.text();
    } else {
      title = item.closest('.rssRow').find('h3 a').text();
    }
    return title;
  }
});