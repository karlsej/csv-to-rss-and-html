# README #

## What is this repository for? ##

* Export a list from Sierra to a text file and create an RSS feed from it.

## Process ##
### Sierra ###
* Run list
* Export (see screenshot in repository)
### Excel ###
  * Import as UTF-8
  * Exclude first line
  * Mark all columns as "text" format
  * Sort by date, z - a
  * Sort by record no., z - a
  * Remove duplicates (based on record number)
  * check to be sure top few look good - delete rows if not. Top few will be auto-posted to Twitter via Twitterfeed
  * Save as Unicode .txt
  * This saves as Unicode LE; Open in Komodo or Notepad++ and save as vanilla UTF-8 

**Note** There's an HTML output script here but I'm not using it or improving it...